<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/7/3
 * Time: 7:56 PM
 */

namespace Ufile\Laravel\Ucloud;


class UcloudGetFile extends Ucloud
{

    protected $type;
    protected $expires = 0;

    public function __construct($bucket, $key, $type)
    {
        $this->bucket = $bucket;
        $this->path = $key;

        $this->type = $type;

        if ($type == 'private'){
            $this->checkConfig(self::GETFILE);
        }
    }

    public function setExpires($time)
    {
        if ($time && is_integer($time)){
            $this->expires = time() + $time;
        }
    }

    public function getFile()
    {
        if (!$this->bucket){
            $this->err[] = new UcloudError(0, -1, "bucket parame missing");
        }
        if (!$this->path){
            $this->err[] = new UcloudError(0, -1, "path parame missing");
        }
        if (!$this->type){
            $this->err[] = new UcloudError(0, -1, "type parame missing");
        }

        if ($this->type == 'public'){

            $url = $this->makePublicUrl();

        } else {

            $url = $this->makePrivateUrl();
        }

        $this->checkErr('UcloudGetFile');

        if ($this->errNo) {
            $content = $this->fileGetContents($url);
            return array(
                'code' => '200',
                'msg' => $content
            );
        } else {

            return array(
                'code' => '700',
                'msg' => 'something is wrong'
            );
        }
    }

    // @results: $url  生成公有文件Url
    protected function makePublicUrl()
    {
        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');
        return $this->bucket . $UCLOUD_PROXY_SUFFIX . "/" . rawurlencode($this->path);
    }


    // @results: $url  生成私有文件Url
    protected function makePrivateUrl()
    {
        $UCLOUD_PUBLIC_KEY = config('ufile.UCLOUD_PUBLIC_KEY');
        $UCLOUD_PRIVATE_KEY = config('ufile.UCLOUD_PRIVATE_KEY');

        $public_url = $this->makePublicUrl();

        $req = new HttpRequest('GET', array('path'=>$public_url), null, $this->bucket, $this->path);

        if ($this->expires > 0) {
            $req->Header['Expires'] = $this->expires;
        }

        $ucloudAuth = new UcloudAuth($UCLOUD_PUBLIC_KEY,$UCLOUD_PRIVATE_KEY);

        $temp = $ucloudAuth->SignRequest($req, null, Ucloud::QUERY_STRING_CHECK);
        $signature = substr($temp, -28, 28);

        $url = $public_url . "?UCloudPublicKey=" . rawurlencode($UCLOUD_PUBLIC_KEY) . "&Signature=" . rawurlencode($signature);
        if ('' != $this->expires) {
            $url .= "&Expires=" . rawurlencode($this->expires);
        }
        return $url;
    }


    //
    protected function fileGetContents($durl)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $durl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true) ; // 获取数据返回
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true) ; // 在启用 CURLOPT_RETURNTRANSFER 时候将获取数据返回
        $r = curl_exec($ch);
        curl_close($ch);
        return $r;
    }
}