<?php
namespace Ufile\Laravel\Ucloud;


class HttpRequest
{
    public $URL;
    public $RawQuerys;
    public $Header;
    public $Body;
    public $UA;
    public $METHOD;
    public $Params;      //map
    public $Bucket;
    public $Key;
    public $Timeout;

    public function __construct($method, $url, $body, $bucket, $key, $action_type = Ucloud::NONE)
    {
        $this->URL    = $url;
        if (isset($url["query"])) {
            $this->RawQuerys = $url["query"];
        }
        $this->Header = array();
        $this->Body   = $body;
        $this->METHOD = $method;
        $this->Bucket = $bucket;
        $this->Key    = $key;

        $this->setUA();

    }

    public function EncodedQuery() {
        if ($this->RawQuerys != null) {
            $q = "";
            foreach ($this->RawQuerys as $k => $v) {
                $q = $q . "&" . rawurlencode($k) . "=" . rawurlencode($v);
            }
            return $q;
        }
        return "";
    }

    public function setUA()
    {
        $SDK_VER = config('ufile.SDK_VER');
        $sdkInfo = "UCloudPHP/$SDK_VER";

        $systemInfo = php_uname("s");
        $machineInfo = php_uname("m");

        $envInfo = "($systemInfo/$machineInfo)";

        $phpVer = phpversion();

        $ua = "$sdkInfo $envInfo PHP/$phpVer";

        $this->UA = $ua;
    }

    public function setTimeOut($timeout)
    {
        $this->Timeout = $timeout;
    }
}



