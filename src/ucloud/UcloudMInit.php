<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/7/4
 * Time: 4:14 PM
 */

namespace Ufile\Laravel\Ucloud;


class UcloudMInit extends Ucloud
{

    public function __construct($bucket, $key)
    {
        $this->bucket = $bucket;
        $this->path = $key;

        $this->key = $key;

        $this->setHost($bucket);
        $this->checkConfig(Ucloud::MINIT);

        $this->query = array("uploads" => "");

        $this->setMimeType('application/x-www-form-urlencoded');

    }

    public function MInit($req)
    {
        if ($this->errNo) {

            $http = new Http();

            list($resp, $err) = $http->UCloud_Client_Do($req);

            if ($err !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            list($data, $errClient) = $http->UCloud_Client_Ret($resp);

            if ($errClient !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            return array(
                'code' => '200',
                'msg' => $data
            );

        } else {

            return array(
                'code' => '702',
                'msg' => 'something is wrong'
            );

        }
    }
}